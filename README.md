# Zadání
Vezměte animované video a vytvořte generátor pro interpolování video rámců, abyste získali vysokofrekvenční zpomalené video. Používejte obě architektury: GAN a U-Net. Porovnejte výsledky.

# Dataset, modely a ukázková videa
[K dispozici zde](https://drive.google.com/file/d/1KrNP6t7ZytbV6rlu9tI1mW-jXWpooY-O/view?usp=sharing),
velikost ~1GB, mělo by být přístupné přes ČVUT FIT účet. Obsahuje použitý dataset, natrénované modely a videa upsamplovaná pomocí těchto modelů.

[Ukázka nejlepšího modelu](https://streamable.com/gpqce)

# Popis zdrojových souborů ve složce src

common.py - obsahuje pomocné funkce

models.py - obsahuje funkce pro vytvoření modelů

splitvideo.py - rozdělí video na trojice snímků pro dataset

train_xy.py - natrénuje model xy na train datasetu

upscale.py - upscaluje framerate videa pomocí nějakého modelu