import keras

from common import *

#creates U-Net model
def unet():
    filters = 32

    input = keras.layers.Input(shape=(None, None, 6))

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(input)
    save1 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save2 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save3 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save4 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save5 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save5, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save4, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save3, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save2, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save1, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    output = keras.layers.Conv2D(3, (1, 1), padding="same", activation = sinac)(conv)

    model = keras.Model(input, output)
    optimizer = keras.optimizers.Adam()
    model.compile(loss=ssim_loss, optimizer=optimizer)
    return model

#creates GAN discriminator model
def def_discriminator():
    filters = 32

    input = keras.layers.Input(shape=(96, 128, 9))

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(input)
    save1 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save2 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save3 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save4 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save5 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save5, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save4, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save3, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save2, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save1, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    flat = keras.layers.Flatten()(conv)

    output = keras.layers.Dense(1, activation = "sigmoid")(flat)

    model = keras.Model(input, output)
    optimizer = keras.optimizers.Adam(learning_rate = 0.00001, amsgrad=True)
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model

#creates GAN model
def def_gan(generator, discriminator):
    discriminator.trainable = False
    ginput = keras.layers.Input(shape=(96, 128, 6))
    gen = generator(ginput)
    dinput = keras.layers.concatenate([ginput, gen], axis = 3)
    disc = discriminator(dinput)

    gan = keras.Model(ginput, disc)
    optimizer = keras.optimizers.Adam(learning_rate = 0.00002, amsgrad=True)
    gan.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return gan

#creates cylic U-Net+C model
def cyclic(generator):
    ginput = keras.layers.Input(shape=(None, None, 6))
    outm = generator(ginput)
    inputl = keras.layers.Lambda(lambda x: keras.backend.slice(x, (0, 0, 0, 0), (-1, -1, -1, 3)))(ginput)
    inputl = keras.layers.concatenate([inputl, outm], axis = 3)
    inputr = keras.layers.Lambda(lambda x: keras.backend.slice(x, (0, 0, 0, 3), (-1, -1, -1, 3)))(ginput)
    inputr = keras.layers.concatenate([outm, inputr], axis = 3)
    outl = generator(inputl)
    outr = generator(inputr)
    oinput = keras.layers.concatenate([outl, outr], axis = 3)
    goutput = generator(oinput)
    
    model = keras.Model(ginput, goutput)
    optimizer = keras.optimizers.Adam(amsgrad=True)
    model.compile(loss=ssim_loss, optimizer=optimizer)
    return model

#creates U-Net block for TU-Net
def def_ublock(filters, channels_in):
    input = keras.layers.Input(shape=(None, None, channels_in))

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(input)
    save1 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save2 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters *= 2
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)
    
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    save3 = conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)
    
    pool = keras.layers.AveragePooling2D(pool_size=(2, 2), padding='same')(conv)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(pool)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)
    
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save3, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save2, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    filters //= 2
    up = keras.layers.UpSampling2D(size=(2, 2), interpolation='bilinear')(conv)
    concat = keras.layers.concatenate([save1, up], axis = 3)

    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(concat)
    conv = keras.layers.Conv2D(filters, (3, 3), padding="same", activation = "relu")(conv)

    output = conv

    model = keras.Model(input, output)

    return model

#creates TU-Net model
def tripleunet():
    filters = 64
    
    input = keras.layers.Input(shape=(None, None, 6))
    inputl = keras.layers.Lambda(lambda x: keras.backend.slice(x, (0, 0, 0, 0), (-1, -1, -1, 3)))(input)
    inputr = keras.layers.Lambda(lambda x: keras.backend.slice(x, (0, 0, 0, 3), (-1, -1, -1, 3)))(input)

    feature_block = def_ublock(filters // 2, 3)
    interpolation_block = def_ublock(filters // 2, 2 * feature_block.output_shape[-1])
    output_block = def_ublock(filters, 2 * feature_block.output_shape[-1] + interpolation_block.output_shape[-1])

    feature1 = feature_block(inputl)
    feature2 = feature_block(inputr)

    features = keras.layers.concatenate([feature1, feature2], axis = 3)
    interpolated = interpolation_block(features)
    
    concat = keras.layers.concatenate([features, interpolated], axis = 3)
    conv = output_block(concat)
    
    conv = keras.layers.Conv2D(3, (1, 1), padding="same", activation = sinac)(conv)

    output = conv

    model = keras.Model(input, output)
    optimizer = keras.optimizers.Adam(amsgrad=True)
    model.compile(loss=ssim_loss, optimizer=optimizer)
    return model
