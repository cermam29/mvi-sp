import numpy as np
import keras
import cv2
from common import *

def inter_frame(gmodel, frame1, frame2):
    ginput = np.concatenate((frame1 / 255.0, frame2 / 255.0), axis=2)
    ginput = np.reshape(ginput, (1,height,width,6))
    gimage = gmodel.predict(ginput)
    gimage = np.reshape(gimage, (height,width,3))
    gimage = np.uint8(gimage * 255)
    return gimage

def interpolate_frames(gmodel, gimage, start, end):
    half = (end - start) // 2
    if half > 0:
        mid = start + half
        gimage[mid] = inter_frame(gmodel, gimage[start], gimage[end])
        interpolate_frames(gmodel, gimage, start, mid)
        interpolate_frames(gmodel, gimage, mid, end)

gmodel = input("model file name: ")

name = input("video file name: ")

width = int(input("output width: "))
height = int(input("output height: "))
fps = int(input("output fps: "))
fmulti = 2**int(input("x times doubled frames: "))

gmodel = keras.models.load_model(gmodel, custom_objects={'sinac': sinac, 'ssim_loss': ssim_loss, 'keras': keras})

vidcap = cv2.VideoCapture(name)

image = [0] * 3
gimage = [0] * (fmulti + 1)
success,image[2] = vidcap.read()
image[2] = cv2.resize(image[2], (width, height), interpolation = cv2.INTER_LANCZOS4)

out = cv2.VideoWriter(name + '_upscaled.mp4',cv2.VideoWriter_fourcc(*'mp4v'), fps, (width*2,height))
out.write(np.concatenate((image[2], image[2]), axis=1))

while True:
    image[0] = image[2]
    success,image[1] = vidcap.read()
    if not success:
        break
    success,image[2] = vidcap.read()
    if not success:
        break
    
    image[2] = cv2.resize(image[2], (width, height), interpolation = cv2.INTER_LANCZOS4)
    
    gimage[0] = image[0]
    gimage[-1] = image[2]

    interpolate_frames(gmodel, gimage, 0, fmulti)

    for i in range(1, fmulti):
        out.write(np.concatenate((image[0], gimage[i]), axis=1))
    out.write(np.concatenate((image[2], image[2]), axis=1))

out.release()