import cv2
import glob
import random
import tensorflow as tf
import numpy as np
from keras.preprocessing.image import load_img, img_to_array, array_to_img, save_img

#custom DSSIM loss function
@tf.function
def ssim_loss(y_true, y_pred):
    return (1 - tf.reduce_mean(tf.image.ssim(y_true, y_pred, 1.0))) / 2

#custom sin^2(x) activation function
@tf.function
def sinac(x):
    return tf.sin(x / 2) * tf.sin(x / 2)

#loads datasets from specified folder
def load_dataset(folder):
    sets = glob.glob(folder + '/*/*/')
    datasetsize = len(sets)
    x = 0
    sample = img_to_array(load_img(sets[0] + "0.png"))
    input = np.ndarray((datasetsize, sample.shape[0], sample.shape[1], sample.shape[2] * 2), np.float32)
    output = np.ndarray((datasetsize, sample.shape[0], sample.shape[1], sample.shape[2]), np.float32)
    for path in sets:
        a = img_to_array(load_img(path + "0.png")) / 255.0
        b = img_to_array(load_img(path + "1.png")) / 255.0
        c = img_to_array(load_img(path + "2.png")) / 255.0
        input[x] = np.concatenate((a, c), axis = 2)
        output[x] = b
        x += 1
    return input, output

#trains model for 1 epoch
def train(model, X_train, Y_train, batch_size):
    datasetsize = X_train.shape[0]
    shift = random.randrange(batch_size)
    batches = list(range((datasetsize - shift) // batch_size))
    random.shuffle(batches)
    done = 0
    outlen = len(model.metrics_names)
    ret = [0] * outlen
    iter = 0
    for batch in batches:
        start = batch * batch_size + shift
        end = (batch + 1) * batch_size + shift
        done += batch_size
        a = model.train_on_batch(X_train[start:end], Y_train[start:end])
        if outlen > 1:
            for i in range(outlen):
                ret[i] += a[i]
        else:
            ret[0] += a
        print(done, end = ' ')
        iter += 1
        for r in ret:
            print("{0:0.6f}".format(r / iter), end = ' ')
        print(end = '\r')
    print()
    for i in range(outlen):
        ret[i] = ret[i] / iter
    return ret