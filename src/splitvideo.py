import cv2
from skimage.metrics import structural_similarity as ssim
import os

def SplitVideo(name, width, height):
    vidcap = cv2.VideoCapture(name)
    i = 0
    iter = 0
    success = True
    image = [0,1,2]
    if not os.path.exists(name):
        os.mkdir(name)
    while True:
        while i < 3:
            success,image[i] = vidcap.read()
            if not success:
                break
            image[i] = cv2.resize(image[i], (width, height), interpolation = cv2.INTER_LANCZOS4)
            if i > 0:
                s = ssim(image[i - 1], image[i], multichannel = True)
                if s < 0.7:
                    image[0] = image[i]
                    i = 1
                elif s < 0.96:
                    i += 1
            else:
                i += 1
        if not success:
            break
        if not os.path.exists("%s/%d" % (name, iter)):
            os.mkdir("%s/%d" % (name, iter))
        cv2.imwrite("%s/%d/0.png" % (name, iter), image[0])
        cv2.imwrite("%s/%d/1.png" % (name, iter), image[1])
        cv2.imwrite("%s/%d/2.png" % (name, iter), image[2])
        image[0] = image[1]
        image[1] = image[2]
        i -= 1
        iter += 1

filename = input("File name: ")
width = input("width: ")
height = input("height: ")
SplitVideo(filename, int(width), int(height))
