from common import *
from models import *

X_train, Y_train = load_dataset('train')
X_val, Y_val = load_dataset('validation')
batch_size = 4
epochs = 30

gmodel = unet()
tloss_file = open("train_loss_history.txt", 'w')
vloss_file = open("eval_loss_history.txt", 'w')

for iter in range(epochs):
    train_loss = train(gmodel, X_train, Y_train, batch_size)[0]
    eval_loss = gmodel.evaluate(X_val, Y_val, batch_size)
    print("({0:0.6f},{1:0.6f})".format(iter + 1, train_loss), end = '', file = tloss_file, flush = True) 
    print("({0:0.6f},{1:0.6f})".format(iter + 1, eval_loss), end = '', file = vloss_file, flush = True) 

tloss_file.close()
vloss_file.close()
gmodel.save("model")