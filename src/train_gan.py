import numpy as np

from common import *
from models import *

X_train, Y_train = load_dataset('train')
X_val, Y_val = load_dataset('validation')
batch_size = 4
epochs = 30

datasetsize = X_train.shape[0]

DX_train = np.concatenate((X_train, Y_train), axis = -1)
DY_train = np.concatenate((np.zeros(datasetsize), np.ones(datasetsize)))

GY_train = np.zeros(datasetsize)

gmodel = unet()
dmodel = def_discriminator()
gan = def_gan(gmodel, dmodel)

vloss_file = open("eval_loss_history.txt", 'w')
dacc_file = open("d_acc_history.txt", 'w')
gacc_file = open("g_acc_history.txt", 'w')

train(gmodel, X_train, Y_train, batch_size)

for iter in range(epochs):
    DX_train2 = gmodel.predict(X_train, batch_size)
    DX_train2 = np.concatenate((X_train, DX_train2), axis = -1)
    DX_train2 = np.concatenate((DX_train, DX_train2), axis = 0)
    dacc = train(dmodel, DX_train2, DY_train, batch_size)[1]
    gacc = train(gan, X_train, GY_train, batch_size)[1]
    eval_loss = gmodel.evaluate(X_val, Y_val, batch_size)
    print("({0:0.6f},{1:0.6f})".format(iter + 1, eval_loss), end = '', file = vloss_file, flush = True) 
    print("({0:0.6f},{1:0.6f})".format(iter + 1, dacc), end = '', file = dacc_file, flush = True) 
    print("({0:0.6f},{1:0.6f})".format(iter + 1, gacc), end = '', file = gacc_file, flush = True) 

gmodel.save("model")
dmodel.save("dmodel")
vloss_file.close()
dacc_file.close()
gacc_file.close()